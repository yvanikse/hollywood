<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./styles/headers.css">
    <link rel="stylesheet" href="./styles/footer.css">
    <link rel="stylesheet" href="./styles/lesEvade.css">
    <title>Les Évadés</title>
</head>

<body>
    <?php
    include './structures/header.php';
    ?>
    <img src="./photo/prisonVueAerien.jpg" class="aeriennePrison" alt="photo de vue aerienne d'une prison">
    <img src="./photo/prisonInterieur.jpg" class="photoPrison" alt="photo de interrieur d'une prison">
    <img src="./photo/prisonCellule.jpg" class="photoPrison" alt="photo d'une cellule de prison">
    <div class="contenaire">
        <h2 class="titreEvades">Les Évadés</h2>
        <p>Dans cette " attraction " vous aurez une seul mission survivre. Pour cela, vous aurez une journée et une nuit afin de réussir a vous évader. Certains gardiens
            sont professionnels mais d'autres auraient plus leur place dans la cellule que vous. Comme dans toute prison qui se " respecte " il y a bien sûr un prisonnier
            qui peut vous obtenir n'importe quoi, du moment que vous avez quelque chose a échanger. Il y a aussi les groupes de prisonniers qui font ce qu'ils veulent
            quand ils veulent. Vous l'aurez compris, mieux vaux baisser les yeux devant eux… ou pas. <br></p>
        <p> Saurez-vous, vous évader à temps ? "Spoiler alerte" : Vous n'avez pas le temps pour attaquer les murs à la petite cuillère. Je vous avais dis de baisser le regard
            devant ce fameux groupe de prisonnier… Cela valait-il vraiment la peine de vous prendre la tête pour garder votre dessert ? J'ai entendu dire
            qu'ils allaient vous rendre visite ce soir à 00h00 au moment des changements de gardiens … <br> Bonne chance. </p>
        <img src="./photo/tenue.png" class="tenue" alt="photo d'une tenu de prisonnier">
    </div>

    <?php
    include './structures/footer.php';
    ?>
</body>

</html>