<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="./styles/headers.css">
    <link rel="stylesheet" href="./styles/footer.css">
    <link rel="stylesheet" href="./styles/accueil.css">
</head>

<body>
    <?php
    include './structures/header.php';
    ?>
    <img class="studio" src="./photo/studio.jpg" alt="photo d'un tournage de film">
    <div id="titre">
        <h1>Plongez dans le monde du cinéma</h1>
    </div>
    <div>
        <p class="textPresentation">
            Entrez dans le monde du cinéma, venez découvrir vos tournages préférés,les secrets de nos plus gros films … <br>
            Plongez dans un univers dont vous n'imaginez même pas l'étendue. Les plus courageux pourront vivre l'espace d'un instant
            les films en conditions réelles.
        </p>

        <div class="lesEvades">
            <img class="photoFilm" src="./photo/evades.jpg" alt="photo de l'affiche du film les évadés">
            <div>
                <a href="lesEvades.php">
                    <h2>Les Évadés</h2>
                    <p>
                        Vous imaginez, vous être fait enfermer à tort pour le reste de votre vie ? C'est ce que notre cher ami et acteur,
                        Tim Robbins a eu le malheur de vivre dans ce magnifique film : les évadés. Mais grâce à son intelligence hors-norme
                        et sa motivation à toute épreuve, il réussit à s'enfuir de cette prison remplie de gardes, achetés, par les détenus.
                        Saurez-vous, vous évader avant qu'il ne soit trop tard ? Sans vous faire attraper ? A vous de jouer …
                    </p>
                </a>
            </div>
        </div>


        <div class="amityville">
            <div>
                <a href="amityville.php">
                    <h2>Amityville</h2>
                    <p>
                        La nuit du 13 novembre 1974, le commissariat du Comté de Suffolk reçut un appel affolé, en provenance
                        de la communauté d'Amityville. Un spectacle d'horreur attendait les policiers dans la résidence du 112
                        "Ocean avenue" : six membres d'une même famille avaient été massacrés.
                        Venez donc passer la nuit... Il nous reste des chambre libres ...

                    </p>
                </a>
            </div>
            <img src="./photo/amityville.jpg" class="photoFilm photoAmityville" alt="photo de l'affiche du film Amityville">
        </div>

        <div class="spider">
            <img src="./photo/Spider.jpg" class="photoFilm" alt="photo de l'affiche du film spider-man">
            <div><a href="">
                    <h2>Spider-man</h2>
                    <p>
                        Orphelin, Peter Parker est élevé par sa tante May et son oncle Ben dans le quartier du Queens à
                        New-York. Poursuivant ses études à l'université, sa vie était normale, jusqu'au jour où, une morsure
                        d'araignée vient modifier son patrimoine génétique.
                        Venez vous mesurer aux capacités de Peter Parker ! Escaladez un bâtiment ? Aiguisez vos réflexes ...
                    </p>
                </a>
            </div>
        </div>
    </div>
    <?php
    include './structures/footer.php';
    ?>
</body>

</html>