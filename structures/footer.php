<footer>
    <div class="footer">
        <div class="connecté">
            <p>Restons connectés</p>
            <a href="https://twitter.com/?lang=fr"><img src="./photo/twitter.png" class="logo" alt="logo twitter"></a>
            <a href="https://www.facebook.com"><img src="./photo/face.png" class="logo" alt="logo facebook"></a>
            <a href="https://www.instagram.com/?hl=fr"><img src="./photo/insta.png" class="logo" alt="logo instagram"></a>
            <a href="https://www.youtube.com"><img src="./photo/youtube.png" class="logo" alt="logo youtube"></a>
        </div>

        <iframe class="carteLocalisation" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d422630.4845434298!2d-118.75202721630372!3d34.151441680505386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf07045279bf%3A0xf67a9a6797bdfae4!2sHollywood%2C%20Los%20Angeles%2C%20Californie%2C%20%C3%89tats-Unis!5e0!3m2!1sfr!2sfr!4v1600778604428!5m2!1sfr!2sfr"></iframe>
        <div class="droits">
            <p>© Tous droits réservés</p>
        </div>
    </div>
</footer>